# Leimonides
## Course completion report plugin for Moodle

This is a course completion report plugin for Moodle, given as a task by Synergy Learning for review

### Tasks:
* [x] The report should appear as a link in the 'reports' section of the "Site administration" navigation
* [x] The report should only be available to site administrators
* [x] When first visiting the report, a list of users on the site should be shown, allowing a single user to be selected
* [x] After selecting a user, a table should be shown. The table lists all the courses that user is enrolled in, with details of their completion state
* [x] The report should contain the following columns:
    * [x] Course name (linked to the main page for that course)
    * [x] Completion status (complete / not complete)   
    * [x] Time the course was completed (formatted as date + time)
* [x] The table should be sorted by course name - there is no need for the sort order to be changeable via the UI
* [x] The code should, as far as possible, follow the Moodle coding style as detailed at https://docs.moodle.org/dev/Coding_style (you may
find it helpful to use the codechecker plugin at https://moodle.org/plugins/local_codechecker )
* [x] The plugin should be pushed to a git repo that we have access to (github is recommended, but any suitable alternative would be
accepted).

### Moodle Coding Style
This release fully meets Moodle's coding style guidelines, below is a screenshot from the codechecker plugin

![codechecker](/uploads/177ffca380c20c47afd249b2114d95d0/codechecker.png)

### Installation
1. Download the latest release here ([course_completion_report.zip](/uploads/835e7062c46eafa6d169886d5c6928fc/course_completion_report.zip))
2. Navigate to Moodle -> Site administration -> Plugins -> Install plugins
3. Install the plugin from ZIP file, choose the zip archive downloaded, and click the button to install the plugin
4. Navigate through installation, confirm you'd like to proceed
5. You should now have the Course Completion Report plugin installed 😊 You can find it under Moodle -> Site Administration -> Reports -> Course completion

![installation](/uploads/8906f62662c1e9db6ae2b3e9fadb42ec/installation.gif)

#### Installation note:
If you manually clone the repository and want to install the plugin, you will need to zip the folder `course_completion_report`, not the parent folder that contains other files like README and LICENSE.

### Where
How to find the plugin:
* Find it under Moodle -> Site Administration -> Reports -> Course completion
* Manually entering in your browser `MOODLE/local/course_completion_report` where `MOODLE` is the URL where your Moodle installation lives e.g: `https://university.edu/moodle/local/course_completion_report`

### Releases:
Below you can find a link to the latest release that is ready to be installed on Moodle

[course_completion_report.zip](/uploads/835e7062c46eafa6d169886d5c6928fc/course_completion_report.zip)