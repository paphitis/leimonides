<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * User page for local plugin course_completion_report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');

// The function admin_externalpage_setup calls require_login and performs
// the permissions checks for admin pages.
admin_externalpage_setup('coursecompletionreport');

// Get the title from our translations strings and also set the URL.
$title = get_string('pluginname', 'local_course_completion_report');
$url = new moodle_url("/local/course_completion_report/user.php");

// Set the page URL, title and heading.
$PAGE->set_url($url);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// Render the page.
$output = $PAGE->get_renderer('local_course_completion_report');

echo $output->header();
echo $output->heading($title);

// User ID.
$id = required_param('id', PARAM_INT);

$data = [];
$data["back_link"] = new moodle_url("/local/course_completion_report/index.php");
$data["courses"] = enrol_get_all_users_courses($id);

$strcomplete = get_string('course_status_complete', 'local_course_completion_report');
$strnotcomplete = get_string('course_status_not_complete', 'local_course_completion_report');
$strna = get_string('na', 'local_course_completion_report');

foreach ($data["courses"] as $course) {
    // Create a completion_completion object to get the time the course was completed.
    $params = array('userid' => $id, 'course' => $course->id);
    $completion = new completion_completion($params);

    // Try get the timestamp the course was completed and format it nicely for
    // displaying to the user.
    $timestamp = $completion->timecompleted;
    $prettydate = userdate($timestamp, get_string('strftimedatetime', 'langconfig'));

    /*
    Add our own two properties to the course object
    status: Status to represent whether the course is completed or not
    time_completed: The time (if the course was completed) in a pretty format
    link: Link to course page
    */
    $course->status = $completion->timecompleted ? $strcomplete : $strnotcomplete;
    $course->time_completed = $timestamp ? $prettydate : $strna;
    $course->link = new moodle_url('/course/view.php', array('id' => $course->id));
}

$renderable = new \local_course_completion_report\output\user_page($data);
echo $output->render($renderable);

echo $output->footer();