<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library class for local plugin course_completion_report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Add link to course completion report to the Reports section under Site administration
 *
 * This is done through the use of the callback local_XXX_extend_settings_navigation
 */
function local_course_completion_report_extend_settings_navigation($settings, $context) {
    global $PAGE;

    // Try find the reports section in the site administration block.
    $reportsnode = $settings->find('reports', navigation_node::TYPE_SETTING);

    // Return if it can't be found.
    if (!$reportsnode) {
        return;
    }

    // Create the link to our report and add it under the reports section.
    $name = get_string('settings_name', 'local_course_completion_report');
    $url = new moodle_url("/local/course_completion_report/index.php");
    $node = navigation_node::create($name, $url, navigation_node::NODETYPE_LEAF);

    $reportsnode->add_node($node);
}