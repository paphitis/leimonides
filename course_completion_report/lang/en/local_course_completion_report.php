<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information for local plugin course_completion_report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Course Completion Report';
$string['settings_name'] = 'Course completion';

$string['back_to_users'] = 'Go back to users';
$string['no_enroled_courses'] = 'No enroled courses';
$string['na'] = 'N/A';

$string['report_course_name_col'] = 'Course name';
$string['report_completion_status_col'] = 'Completion status';
$string['report_time_course_completed_col'] = 'Time the course was completed';

$string['course_status_complete'] = 'Complete';
$string['course_status_not_complete'] = 'Not complete';