<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings for local plugin course_completion_report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

// Add an external admin page which can be used later when
// checking the user is an admin when accessing the page
//
// The task of adding our report to the Reports section under Site Administration
// has been completed, so instead we add this page under the Courses section,
// this way the two don't interfere.
$ADMIN->add("courses",
        new admin_externalpage(
                'coursecompletionreport',
                get_string('pluginname', 'local_course_completion_report'),
                "$CFG->wwwroot/local/course_completion_report/index.php"
        )
);