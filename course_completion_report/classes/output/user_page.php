<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * User page for local plugin course_completion_report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_course_completion_report\output;

defined('MOODLE_INTERNAL') || die;

use renderable;
use renderer_base;
use templatable;
use stdClass;
use ArrayIterator;

/**
 * User page renderable
 */
class user_page implements renderable, templatable {
    private $data = null;

    public function __construct($data) {
        $this->data = $data;
    }

    public function export_for_template(renderer_base $output) {
        $data = new stdClass();

        $data->back_link = $this->data["back_link"];
        $data->courses = new ArrayIterator($this->data["courses"]);
        $data->no_courses = count($data->courses) ? false : true;

        return $data;
    }
}