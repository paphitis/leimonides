<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main class for local plugin course_completion_report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');

// The function admin_externalpage_setup calls require_login and performs
// the permissions checks for admin pages.
admin_externalpage_setup('coursecompletionreport');

// Get the title from our translations strings and also set the URL.
$title = get_string('pluginname', 'local_course_completion_report');
$url = new moodle_url("/local/course_completion_report/index.php");

// Set the page URL, title and heading.
$PAGE->set_url($url);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// Render the page.
$output = $PAGE->get_renderer('local_course_completion_report');

echo $output->header();
echo $output->heading($title);

// Get the user data.
$data = [];
$data["users"] = $DB->get_records('user');

// Add full name and link to individual user course page to user object.
foreach ($data["users"] as $user) {
    $user->full_name = fullname($user);
    $user->link = new moodle_url('/local/course_completion_report/user.php', array('id' => $user->id));
}

$renderable = new \local_course_completion_report\output\index_page($data);
echo $output->render($renderable);

echo $output->footer();