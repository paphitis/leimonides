<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for Course completion report
 *
 * @package local_course_completion_report
 * @author Alex Paphitis <alex@paphitis.net>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_course_completion_report\output;

defined('MOODLE_INTERNAL') || die;

use plugin_renderer_base;

/**
 * Class for rendering index page
 */
class renderer extends plugin_renderer_base {
    /**
     * Render index page
     *
     * @param index_page $page
     * @return string HTML for index page
     */
    public function render_index_page($page) {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_course_completion_report/index_page', $data);
    }

    /**
     * Render user page
     *
     * @param index_page $page
     * @return string HTML for user page
     */
    public function render_user_page($page) {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_course_completion_report/user_page', $data);
    }
}